\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{mathtools}
\usepackage{amssymb}
\usepackage{graphicx}

\linespread{1.5}
\makeatletter
\renewcommand*\env@matrix[1][\arraystretch]{%
	\edef\arraystretch{#1}%
	\hskip -\arraycolsep
	\let\@ifnextchar\new@ifnextchar
	\array{*\c@MaxMatrixCols c}}
\makeatother

\newcommand{\R}{\mathbb{R}}
\newcommand{\equil}{(x_\infty, y_\infty)}
\newcommand{\tr}{\text{tr}}
\newcommand{\xinfty}{x_\infty}
\newcommand{\yinfty}{y_\infty}
\newcommand{\bld}[1]{\textbf{#1}}
\newcommand{\pdif}[2]{\frac{\partial #1}{\partial #2}}

\newtheorem{defn}{Definition}[subsection]
\newtheorem*{lemma}{Lemma}
\newtheorem*{cor}{Corollary}
\newtheorem*{theorem}{Theorem}


\author{Kirstin Hohls, Bafana Mathebula, Jaco Stroebel }
\title{Kolmogorov type model of mutualistic system}
\begin{document}
	\maketitle
	
	\section{Model}
	\begin{equation}
	\begin{aligned}\label{eq:Model}
	x' = xf(x, y) \\ y' = yg(x, y)
	\end{aligned}\tag{2.7.1}
	\end{equation}
	To describe the mutualistic effect of the $y$-species on the $x$-species, assume that 
	\begin{equation}\label{eq:assump}
	f_x(x, y) < 0,\qquad f_y(x, y) \ge 0,\qquad x, y \ge 0\tag{2.7.2}
	\end{equation}
	In the case that $f_y(x, y) = 0$ for all $x$ and $y$, the effect is called \emph{commensualism}.
	\begin{defn}[Facultative]
		 The two interacting species can survive separately.
	\end{defn} 
	
	\begin{defn}[Obligatory]
		 Each species will become extinct without the existence of the other.
	\end{defn}
	
	Since we have that $f_x(x, y) < 0$, we can describe the $x$-isocline, $f(x, y) = 0$ as $\phi(y) = x$, due to the inverse function theorem. Similarly, the $y$-isocline, $g(x, y) = 0$, can be described as $\psi(x) = y$.
	
	We also have 
	\begin{align}
	\phi(y) \ge 0 \ \text{with } \phi'(y) \ge 0,\ 0 \le \alpha \le y < + \infty\tag{2.7.8}\label{eq:phi}\\
	\psi(x) \ge 0 \ \text{with } \psi'(x) \ge 0,\ 0 \le \beta \le x < + \infty\tag{2.7.9}\label{eq:psi}
	\end{align}
	
	To prevent the mutualistic effect to cause the populations to increase unboundedly, assume 
	\begin{align*}
	\phi (\infty) &\coloneqq \lim\limits_{y \rightarrow \infty}\phi(y) = K^* < + \infty\tag{2.7.8}\label{eq:mutuallimx}\\
	\psi(\infty) &\coloneqq \lim\limits_{x \rightarrow \infty}\psi(x) = M^* < +\infty \tag{2.7.9}	\label{eq:mutuallimy}
	\end{align*}
	
	Notice that in the case of $\alpha = 0$ and $\phi(0) = K >0$ \emph{i.e.} $f(K,0) = 0$ means that the $x$-species is facultative and $K$ is the carrying capacity. If $\alpha > 0$ or $\alpha = 0$ but $\phi(0) = 0$, the $x$-species is obligatory.
	
	The $y$-species is facultative if $\beta = 0$ and $\psi (0) = M >0$, so that $g(0,M) = 0$. If $\beta > 0$ or $\beta = 0$ but $\psi (0) = 0$, the $y$-species is obligatory.
	
	The conditions \eqref{eq:mutuallimx} and \eqref{eq:mutuallimy}, which are not true in the linear case, guarantee that the mutualistic effect of either population is not so strong as to allow the other species' population to become unbounded.
	
	\begin{equation*}
	K^* > K \qquad \text{and} \qquad M^* > M
	\end{equation*} since $\phi(y)$ and $\psi(x)$ are increasing functions. We can regard $K^*$ as an increased carrying capacity for the $x$-species produced by the mutualistic effect of the $y$-species.
\	
	\section{General condition for interior equilibrium}
	The Jacobian given by the system in \eqref{eq:Model} is
	\begin{align*}
	J = \begin{bmatrix}[1.5]
	f(x, y) + xf_x(x, y) & xf_y(x, y) \\ yg(x, y) & g(x, y) + yg_y(x, y)
	\end{bmatrix}.
	\end{align*}
	At an, assumed, interior equilibrium point,
	\[
	J\equil = \begin{bmatrix}[1.5]
	\xinfty f_x\equil & \xinfty f_y\equil \\ \yinfty g_x\equil &\yinfty g_y\equil
	\end{bmatrix}
	\]
	
	With $$\tr J = \xinfty f_x\equil + \yinfty g_y\equil < 0,$$ since $f_x(x, y) \text{ \& } g_y(x,y) < 0$;
	\[
	\det J = \xinfty\yinfty [f_x\equil g_y\equil - f_y\equil g_x\equil];
	\] and
	\[
	\Delta = [\xinfty f_x\equil - \yinfty g_y\equil]^2 + 4 \xinfty\yinfty f_y\equil g_x\equil \ge 0.
	\]
	
	\begin{lemma}[2.7.4]
		$\det J > 0 \Leftrightarrow $ the crossing of the curves $f(x, y) = 0$ and $g(x, y) = 0$ at $\equil$ is such that the curve $f(x, y) = 0$ is above the curve $g(x, y) = 0$ to the right of $\xinfty$:
		\[
		-\frac{f_x\equil}{f_y\equil} > - \frac{g_x\equil}{g_y\equil}\tag{2.7.15}
		\]
	\end{lemma}
\begin{proof}
	Consequence of the expression of $\det J$ - \emph{c.f.} table at Theorem 2.3.7 using Theorem 2.3.8.
\end{proof}

\begin{cor}[2.7.5]
	An interior equilibrium $\equil$ is either a saddle point or an asymptotically stable node.
\end{cor}
\begin{proof}
	Since $\Delta \ge 0$ and $\tr J < 0$, the Hartman-Grobman theorem tells us that $\equil$ is an asymptotically stable node if $\det J > 0$ or a saddle point if $\det J < 0$.
\end{proof}

\paragraph{Remark 2.7.6} The analogue of Lemma 2.7.4 for competition models was considered in Lemma 2.5.10. There the inequality (2.7.15) was in the opposite direction and was called the ``coexistence slope relation''. Also, the curve $f(x, y) = 0$ was below $g(x, y) = 0$ at the right of $\xinfty$ for asymptotic stability.
	
	\section{Property of alternate equilibrium points}
	\begin{cor}[2.7.7]
		Interior equilibria must alternate between saddle points and asymptotically stable nodes if the curves $f(x, y) = 0$ and $g(x, y) = 0$ are not tangent at any interior equilibrium point.
	\end{cor}
\begin{proof}
	This is a consequence of (2.7.4) and Corollary 2.7.5. Indeed, let $(\xinfty^1, \yinfty^1)$ and $(\xinfty^2, \yinfty^2)$ be two consecutive interior equilibria. If the curve $f(x, y) = 0$ is above the curve $g(x, y) = 0 $ at the right of $\xinfty^2$, so that $(\xinfty^2, \yinfty^2)$ is an asymptotically stable node, then the curve $f(x, y) = 0$ was below the curve $g(x, y)$ at the right of $\xinfty^1$, so that $(\xinfty^1, \yinfty^1)$ was a saddle point.
\end{proof}
	\section{Existence of interior equilibrium points}
\begin{theorem}[2.7.8]
	Assume that an interior equilibrium exists. Then, there must be a last equilibrium, which is an asymptotically stable node, and which is characterised by having the largest values of $x$ and $y$;
	
	If $g(x, y) = 0$ is above $f(x, y) = 0$ for small $x$, there exists at least one interior equilibrium point.
\end{theorem}
\begin{proof}
	The first bullet is a consequence of the fact that the line $x = K^*$ is a vertical asymptote of the curve $f(x, y) = 0$, while the line $y = M^*$ is  a horizontal asymptote of the curve $g(x, y) = 0$. The asymptotic stability of the equilibrium follows from Lemma 2.7.4 and Corollary 2.7.5.
\end{proof}
\end{document}